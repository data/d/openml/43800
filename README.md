# OpenML dataset: Microsoft-Stock-market-(2001---2021)

https://www.openml.org/d/43800

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Content
Microsoft is an American multinational technology company.  It develops, manufactures, licenses, supports, and sells computer software, consumer electronics, personal computers, and related services. Its best known software products are the Microsoft Windows line of operating systems, the Microsoft Office suite, and the Internet Explorer and Edge web browsers.
So , For business and stock market, here is Microsoft Stock market from 2001 to the beginning of 2021 . This data well help you to practice the real world TimeSeries analysis and prediction. 
**Note **: double check the time frequency , you might find something like filling some values  .
Acknowledgements
The data is taken from twelvedata.com
And thanks to GA tean for this project.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43800) of an [OpenML dataset](https://www.openml.org/d/43800). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43800/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43800/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43800/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

